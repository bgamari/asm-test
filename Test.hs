module Test where

test :: Int -> Int
test n = n + 1
{-# NOINLINE test #-}
