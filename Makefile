HC ?= ghc
HCOPTS += -ddump-to-file -ddump-asm -ddump-stg

all : Main

clean :
	git clean -f

Test.o : HCOPTS+=-fasm

Main.o : Test.o

Main : Main.o Test.o
	${HC} -o $@ $+

%.o : %.hs
	${HC} -c ${HCOPTS} -o $@ $<

